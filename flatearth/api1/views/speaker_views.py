from rest_framework import generics, status
from rest_framework.response import Response

from core.models import Speaker
from ..serializers import SpeakerSerializer


class SpeakersView(generics.ListAPIView):
    serializer_class = SpeakerSerializer

    def get_queryset(self):
        qs = Speaker.objects.all()

        return qs


class SpeakerView(generics.RetrieveAPIView):
    serializer_class = SpeakerSerializer

    def get_object(self):
        id = self.kwargs.get('id', None)
        if not id:
            return Response(
                data={'error': 'Invalid speaker ID.'},
                status=status.HTTP_400_BAD_REQUEST
            )

        try:
            obj = Speaker.objects.get(id=id)
        except Speaker.DoesNotExist:
            return Response(
                data={'error': 'Speaker with given ID not found.'},
                status=status.HTTP_404_NOT_FOUND
            )

        return obj
