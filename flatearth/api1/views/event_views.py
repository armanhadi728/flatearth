from rest_framework import generics, status
from rest_framework.exceptions import bad_request

from core.models import Event
from ..serializers import EventSerializer


class EventsView(generics.ListAPIView):
    serializer_class = EventSerializer

    def get_queryset(self):
        qs = Event.objects.all()

        parameters = self.request.GET

        return qs


class EventView(generics.RetrieveAPIView):
    serializer_class = EventSerializer

    def get_object(self):
        id = self.kwargs.get('id', None)
        if not id:
            return bad_request(
                data={'error': 'Invalid event ID.'},
                status=status.HTTP_400_BAD_REQUEST
            )

        try:
            obj = Event.objects.get(id=id)
        except Event.DoesNotExist:
            return bad_request(
                data={'error': 'Event with given ID not found.'},
                status=status.HTTP_404_NOT_FOUND
            )

        return obj
