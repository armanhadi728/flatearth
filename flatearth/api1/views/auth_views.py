from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.settings import api_settings

from ..serializers import AuthorizeWithOTPSerializer


class CreateAuthTokenView(ObtainAuthToken):
    '''Create auth token for user'''
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES


class AuthorizeWithOTPView(ObtainAuthToken):
    """Authorize user with OTP"""
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES
    serializer_class = AuthorizeWithOTPSerializer
