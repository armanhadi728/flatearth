from rest_framework import generics, status
from rest_framework.response import Response

from core.models import Sponser
from ..serializers import SponserSerializer


class SponsersView(generics.ListAPIView):
    serializer_class = SponserSerializer

    def get_queryset(self):
        qs = Sponser.objects.all()

        return qs


class SponserView(generics.RetrieveAPIView):
    serializer_class = SponserSerializer

    def get_object(self):
        id = self.kwargs.get('id', None)
        if not id:
            return Response(
                data={'error': 'Invalid sponser ID.'},
                status=status.HTTP_400_BAD_REQUEST
            )

        try:
            obj = Sponser.objects.get(id=id)
        except Sponser.DoesNotExist:
            return Response(
                data={'error': 'Sponser with given ID not found.'},
                status=status.HTTP_404_NOT_FOUND
            )

        return obj
