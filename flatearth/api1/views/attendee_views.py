from rest_framework import generics, status
from rest_framework.exceptions import bad_request
from rest_framework.response import Response

from core.models import Attendee
from ..serializers import AttendeeSerializer


class AttendeesView(generics.ListAPIView):
    serializer_class = AttendeeSerializer

    def get_queryset(self):
        qs = Attendee.objects.all()

        return qs


class AttendeeView(generics.RetrieveAPIView):
    serializer_class = AttendeeSerializer

    def get_object(self):
        id = self.kwargs.get('id', None)
        if not id:
            return Response(
                data={'error': 'Invalid attendee ID.'},
                status=status.HTTP_400_BAD_REQUEST
            )

        try:
            obj = Attendee.objects.get(id=id)
        except Attendee.DoesNotExist:
            return Response(
                data={'error': 'Attendee with given ID not found.'},
                status=status.HTTP_404_NOT_FOUND
            )

        return obj
