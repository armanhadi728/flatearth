from rest_framework import generics, status
from rest_framework.response import Response

from core.models import Member
from ..serializers import MemberSerializer


class MembersView(generics.ListAPIView):
    serializer_class = MemberSerializer

    def get_queryset(self):
        qs = Member.objects.all()

        return qs


class MemberView(generics.RetrieveAPIView):
    serializer_class = MemberSerializer

    def get_object(self):
        id = self.kwargs.get('id', None)
        if not id:
            return Response(
                data={'error': 'Invalid member ID.'},
                status=status.HTTP_400_BAD_REQUEST
            )

        try:
            obj = Member.objects.get(id=id)
        except Member.DoesNotExist:
            return Response(
                data={'error': 'Member with given ID not found.'},
                status=status.HTTP_404_NOT_FOUND
            )

        return obj
