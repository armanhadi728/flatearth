from threading import Thread

from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from core.models import WebhookLog, Attendee
from utils.error_logger import log_error


def hook_to_instance(data):
    att = Attendee()
    att.first_name = data.get('data[first_name]', '')
    att.last_name = data.get('data[last_name]', '')
    att.email = data.get('data[email]', '')
    att.phone_number = data.get('data[mobile]', None)
    att.event_name = data.get('data[ticket][data][event_id]', '')
    att.introduction = data.get('data[answers][data][0][value]', '')
    att.save()


class WebhookView(APIView):
    """A webhook for landing page"""
    authentication_classes = ()
    permission_classes = (AllowAny,)

    def do_webhook_async(self):
        WebhookLog.objects.delete_olds()
        try:
            hook = WebhookLog.objects.create(
                data=str(self.data),
                comes_from='evand'
            )
            hook_to_instance(self.data)
        except Exception as e:
            log_error(e)

    def post(self, request):
        try:
            self.data = request.data
            Thread(target=self.do_webhook_async).start()
        except Exception as e:
            log_error(e)
        finally:
            return Response('OK', status=status.HTTP_200_OK)
