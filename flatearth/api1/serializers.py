from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _

from rest_framework import serializers

from core import models
from utils.regulators import regulate_phone_number
from utils.error_logger import log_error
from utils.generators import OTP_generator


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        exclude = ['password',]


class MemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Member
        fields = '__all__'


class SponserSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Sponser
        fields = '__all__'


class SpeakerSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Speaker
        fields = '__all__'


class EventSerializer(serializers.ModelSerializer):
    speakers = SpeakerSerializer(many=True)
    sponsers = SponserSerializer(many=True)
    jheld_on = serializers.DateTimeField(read_only=True)

    class Meta:
        model = models.Event
        fields = '__all__'


class AttendeeSerializer(serializers.ModelSerializer):
    jdate_joined = serializers.DateTimeField(read_only=True)

    class Meta:
        model = models.Attendee
        fields = '__all__'


class AuthorizeWithOTPSerializer(serializers.Serializer):
    phone_number = serializers.CharField(
        label=_("Unique ID"),
        write_only=True,
        required=False
    )
    email = serializers.CharField(
        label=_("Email"),
        write_only=True,
        required=False
    )
    code = serializers.CharField(
        label=_("Code"),
        style={'input_type': 'password'},
        trim_whitespace=False,
        write_only=True
    )
    token = serializers.CharField(
        label=_("Token"),
        read_only=True
    )

    def authorize(self, code, unique_id, student=None):
        otp_set = models.OTP.objects.filter(unique_id=unique_id)
        for otp in otp_set:
            if otp.check_code(code):
                break
        else:
            raise serializers.ValidationError(
                _('Invalid code.'), code='authorization')

        if student:
            if not student.user:
                try:
                    student.user = get_user_model().objects.create(
                        first_name=student.first_name,
                        last_name=student.last_name,
                        email=student.email if student.email else '',
                        username=f'{student.id}.{unique_id}',
                        password=OTP_generator(
                            code_type='alphanumeric', max_length=8)
                    )
                    student.save()
                except Exception as e:
                    log_error(e)
                    raise serializers.ValidationError(
                        str(e), code='authorization')
            otp_set.delete()
            return student.user
        otp_set.delete()
        return True

    def validate(self, attrs):
        phone_number = attrs.get('phone_number')
        email = attrs.get('email')
        code = attrs.get('code')

        if phone_number:
            phone_number = regulate_phone_number(phone_number)
            try:
                student = models.Student.objects.filter(
                    phone_number=phone_number)[0]
            except Exception as e:
                log_error(e)
                raise serializers.ValidationError(
                    _('Invalid phone_number.'))

            attrs['user'] = self.authorize(code, phone_number, student)
        elif email:
            try:
                student = models.Student.objects.filter(
                    email=email)[0]
            except Exception as e:
                log_error(e)
                raise serializers.ValidationError(
                    _('Invalid email.'))

            attrs['user'] = self.authorize(code, email, student)
        else:
            raise serializers.ValidationError(
                _('Either phone_number or email must be included.'))

        
        return attrs
