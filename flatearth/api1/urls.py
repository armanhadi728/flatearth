from django.urls import path

from rest_framework.schemas import get_schema_view

from . import views


urlpatterns = [
    # Schema
    path('openapi', get_schema_view(
        title="Your Project",
        description="API for all things …",
        version="1.0.0"
    ), name='openapi-schema'),

    # Webhook
    path('webhook_1389712498072',
        views.WebhookView.as_view(), name='webhook'),

    # Event Endpoints
    path('events',
        views.EventsView.as_view(), name='events'),
    path('event/<int:id>',
        views.EventView.as_view(), name='event-id'),

    # Member Endoints
    path('members',
        views.MembersView.as_view(), name='members'),
    path('member/<int:id>',
        views.MemberView.as_view(), name='member-id'),

    # Sponser Endpoints
    path('sponsers',
        views.SponsersView.as_view(), name='sponsers'),
    path('sponser/<int:id>',
        views.SponserView.as_view(), name='sponser-id'),

    # Speaker Endpoints
    path('speakers',
        views.SpeakersView.as_view(), name='speakers'),
    path('speaker/<int:id>',
        views.SpeakerView.as_view(), name='speaker-id'),

    # Attendee Endpoints
    path('attendees',
        views.AttendeesView.as_view(), name='attendees'),
    path('attendee/<int:id>',
        views.AttendeeView.as_view(), name='attendee-id'),
]
