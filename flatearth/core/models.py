from zoneinfo import ZoneInfo

from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import get_user_model
from django.utils import timezone
from django.contrib.auth.hashers import make_password, check_password

from jdatetime import datetime as jdt

from utils.generators import OTP_generator


OTP_CODE_MAX_LENGTH = 5


def get_jdatetime(_d: timezone.datetime) -> jdt:
    if not _d:
        return None
    return jdt.fromgregorian(datetime=_d).astimezone(ZoneInfo('Asia/Tehran'))

def set_jdatetime(obj, attribute, _d: timezone.datetime):
    if not _d:
        return
    setattr(obj, attribute, jdt.togregorian(_d).astimezone(ZoneInfo('UTC')))


def get_directory_path(instacne, filename):
    if isinstance(instacne, Event):
        return f'Events/{instacne.id}/image/{filename}'
    elif isinstance(instacne, Speaker):
        return 'Events/{event}/Speakers/{speaker}/image/{filename}'.format(
            event=instacne.event.id,
            speaker=instacne.id,
            filename=filename
        )
    elif isinstance(instacne, Sponser):
        return 'Events/{event}/Sponsers/{sponser}/image/{filename}'.format(
            event=instacne.event.id,
            sponser=instacne.id,
            filename=filename
        )
    elif isinstance(instacne, Member):
        return f'Members/{instacne.id}/image/{filename}'


class OTPManager(models.Manager):
    def generate_and_create(self, unique_id, code_type='numeric'):
        code = OTP_generator(
            code_type=code_type, max_length=OTP_CODE_MAX_LENGTH)

        instance = self.create(code=code, unique_id=unique_id)
        return code, instance

    def delete_olds(self, deltatime=timezone.timedelta(hours=12)):
        return self.filter(
            created_at__lte=timezone.now() - deltatime
        ).delete()

class OTP(models.Model):
    code = models.CharField(
        verbose_name=_('code'),
        max_length=255)
    unique_id = models.CharField(
        verbose_name=_('unique_id'), max_length=50)
    created_at = models.DateTimeField(
        verbose_name=_('created_at'), default=timezone.now, blank=True)

    objects = OTPManager()

    class Meta:
        ordering = ['created_at', 'unique_id']
        indexes = [
            models.Index(fields=['created_at',]),
            models.Index(fields=['unique_id',]),
        ]

    def check_code(self, code):
        return check_password(code, self.code)

    def save(self, *args, **kwargs):
        self.code = make_password(self.code)
        self.__class__.objects.filter(unique_id=self.unique_id).delete()
        super().save(*args, **kwargs)

    def __str__(self):
        return f"{self.unique_id}'s OTP"


class WebhookLogManager(models.Manager):
    def delete_olds(self, deltatime=timezone.timedelta(days=7)):
        return self.filter(
            received_at__lte=timezone.now() - deltatime
        ).delete()

class WebhookLog(models.Model):
    data = models.TextField(
        verbose_name=_('JSON data'), default='', blank=True)
    comes_from = models.CharField(
        verbose_name=_('From where comes'), max_length=100,
        null=True, blank=True)
    received_at = models.DateTimeField(
        verbose_name=_('Created at'), default=timezone.now, blank=True)

    objects = WebhookLogManager()

    class Meta:
        ordering = ['-received_at', 'comes_from']
        indexes = [
            models.Index(fields=['received_at',]),
            models.Index(fields=['comes_from',]),
        ]

    def __str__(self):
        return f'{self.comes_from} - {self.received_at}'


class Event(models.Model):
    name = models.CharField(
        max_length=250)
    detail = models.CharField(
        max_length=500, blank=True, null=True)
    description = models.TextField(
        blank=True, null=True)
    image = models.ImageField(
        upload_to=get_directory_path, null=True, blank=True)
    video_link = models.CharField(
        max_length=500, null=True, blank=True)

    held_on = models.DateTimeField(
        null=True, blank=True)

    def jheld_on(self):
        return get_jdatetime(self.held_on)

    def __str__(self):
        return self.name


class Speaker(models.Model):
    event = models.ForeignKey(
        Event, on_delete=models.CASCADE,
        related_name='speakers', related_query_name='speaker')

    name = models.CharField(
        max_length=250)
    detail = models.CharField(
        max_length=500, blank=True, null=True)
    description = models.TextField(
        blank=True, null=True)
    image = models.ImageField(
        upload_to=get_directory_path, null=True, blank=True)
    video_link = models.CharField(
        max_length=500, null=True, blank=True)

    def __str__(self):
        return self.name


class Sponser(models.Model):
    event = models.ForeignKey(
        Event, on_delete=models.CASCADE,
        related_name='sponsers', related_query_name='sponser')

    name = models.CharField(
        max_length=250)
    detail = models.CharField(
        max_length=500, blank=True, null=True)
    description = models.TextField(
        blank=True, null=True)
    image = models.ImageField(
        upload_to=get_directory_path, null=True, blank=True)
    video_link = models.CharField(
        max_length=500, null=True, blank=True)

    def __str__(self):
        return self.name


class Member(models.Model):
    name = models.CharField(
        max_length=250)
    position = models.CharField(
        max_length=250, blank=True, null=True)
    image = models.ImageField(
        upload_to=get_directory_path, null=True, blank=True)

    user = models.OneToOneField(
        get_user_model(), on_delete=models.SET_NULL, null=True,
        related_name='members', related_query_name='member')

    # Social
    instagram = models.CharField(
        max_length=250, blank=True, null=True)
    linkedin = models.CharField(
        max_length=250, blank=True, null=True)
    twitter = models.CharField(
        max_length=250, blank=True, null=True)

    def __str__(self):
        return self.name


class Attendee(models.Model):
    phone_number = models.CharField(
        verbose_name='phone_number', max_length=20,
        null=True, blank=True)
    user = models.OneToOneField(
        get_user_model(), on_delete=models.SET_NULL,
        null=True, blank=True)
    date_joined = models.DateTimeField(
        verbose_name='date joined', default=timezone.now,
        blank=True, null=True)
    event = models.ForeignKey(
        Event, on_delete=models.SET_NULL,
        null=True, blank=True,
        related_name='attendees', related_query_name='attendee')
    
    email = models.EmailField(
        verbose_name='email', max_length=100,
        blank=True, default='')
    first_name = models.CharField(
        verbose_name='first name', max_length=100,
        blank=True, default='')
    last_name = models.CharField(
        verbose_name='last name', max_length=100,
        blank=True, default='')
    birth_date = models.DateField(
        verbose_name='birth_date',
        blank=True, null=True)
    city = models.CharField(
        verbose_name='city', max_length=100,
        blank=True, null=True)
    education = models.CharField(
        verbose_name='education', max_length=100,
        blank=True, null=True)
    about_me = models.CharField(
        verbose_name='about me', max_length=500,
        blank=True, null=True)
    event_name = models.CharField(
        verbose_name='event name', max_length=100,
        blank=True, default='')
    introduction = models.CharField(
        verbose_name='introduction', max_length=100,
        blank=True, default='')

    def add_user(self):
        _user, created = get_user_model().objects.get_or_create(
            username=self.phone_number,
            defaults={'password': self.phone_number}
        )
        _user.first_name = self.first_name
        _user.last_name = self.last_name
        _user.email = self.email
        _user.save()

        return _user

    def jdate_joined(self):
        return get_jdatetime(self.date_joined)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'
