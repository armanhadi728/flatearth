from django.contrib import admin

from . import models


@admin.register(models.WebhookLog)
class WebhookLogAdmin(admin.ModelAdmin):
    ordering = ['-received_at', 'comes_from']
    list_display = ['received_at', 'comes_from']
    list_filter = ['received_at', 'comes_from']
    search_fields = ['data', 'comes_from']


@admin.register(models.Event)
class EventAdmin(admin.ModelAdmin):
    ordering = ('-held_on', 'name',)
    list_display = ('name', 'jheld_on', 'id')
    search_fields = ('name', 'detail', 'description')
    list_filter = ('held_on',)


@admin.register(models.Speaker)
class SpeakerAdmin(admin.ModelAdmin):
    ordering = ('name', 'event')
    list_display = ('name', 'event', 'id')
    search_fields = ('name', 'detail', 'description', 'event')
    list_filter = ('event',)


@admin.register(models.Sponser)
class SponserAdmin(admin.ModelAdmin):
    ordering = ('name', 'event')
    list_display = ('name', 'event', 'id')
    search_fields = ('name', 'detail', 'description', 'event')
    list_filter = ('event',)


@admin.register(models.Member)
class MemberAdmin(admin.ModelAdmin):
    ordering = ('name', 'position')
    list_display = ('name', 'position', 'id')
    search_fields = ('name', 'position')
    list_filter = ('position',)


@admin.register(models.Attendee)
class AttendeeAdmin(admin.ModelAdmin):
    ordering = ('-date_joined', 'first_name', 'last_name')
    list_display = (
        'phone_number', 'first_name', 'last_name',
        'jdate_joined', 'introduction'
    )
    search_fields = (
        'first_name', 'last_name', 'email',
        'phone_number', 'city', 'education'
    )
    list_filter = (
        'date_joined', 'birth_date', 'city',
        'education', 'event_name', 'introduction'
    )
