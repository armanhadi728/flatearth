# Generated by Django 4.1.3 on 2023-02-05 17:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0014_attendee_event_attendee_event_name_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attendee',
            name='event_name',
            field=models.CharField(blank=True, default='', max_length=100, verbose_name='event name'),
        ),
        migrations.AlterField(
            model_name='attendee',
            name='introduction',
            field=models.CharField(blank=True, default='', max_length=100, verbose_name='introduction'),
        ),
    ]
