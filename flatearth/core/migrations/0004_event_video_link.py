# Generated by Django 4.1.3 on 2022-12-30 09:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_alter_event_image_alter_member_image_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='video_link',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
    ]
