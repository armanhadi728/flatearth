# Generated by Django 4.1.3 on 2022-12-30 09:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_event_video_link'),
    ]

    operations = [
        migrations.AddField(
            model_name='speaker',
            name='video_link',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
        migrations.AddField(
            model_name='sponser',
            name='video_link',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
    ]
