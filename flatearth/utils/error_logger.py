from zoneinfo import ZoneInfo
import traceback
from datetime import datetime

from django.conf import settings



def log_error(e):
    with open(settings.BASE_DIR / '.errorlog', 'a') as f:
        tb = traceback.format_exc()
        dt = datetime.now().astimezone(tz=ZoneInfo('Asia/Tehran'))
        f.write(f"""{str(dt)}:
{''.join(tb)}
------------------------------------------------------------------------------

""")
