from random import choice


def OTP_generator(code_type='numeric', max_length=5):
    if not (\
        code_type == 'numeric' or \
        code_type == 'alpha' or \
        code_type == 'alphanumeric'):
        raise Exception(
            'code_type must be `alpha` or `numeric` or `alphanumeric`.')
    code_types = {
        'numeric': '0123456789',
        'alpha': 'abcdefghijklmnopqrstuvwxyz',
        'alphanumeric': 'aefgjklmpsuwxyz0123456789',
    }

    code = ''
    for i in range(max_length):
        code += choice(list(code_types[code_type]))

    return code
