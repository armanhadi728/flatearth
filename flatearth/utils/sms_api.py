import requests
from threading import Thread

from django.conf import settings

from utils.error_logger import log_error
from utils.regulators import regulate_phone_number


class SMS():
    rest_url = 'https://rest.payamak-panel.com/api/SendSMS'
    send_endpoint = rest_url + '/SendSMS'
    base_send_endpoint = rest_url + '/BaseServiceNumber'

    def __init__(self, username, password, *args, **kwargs):
        self.username = username
        self.password = password

    def send(self, _from, to, text, isflash=False):
        try:
            for i in range(3):
                res = requests.post(
                    url=self.send_endpoint,
                    data={
                        'username': self.username,
                        'password': self.password,
                        'from': _from,
                        'to': to,
                        'text': text,
                        'isflash': isflash
                    }
                )
                status = res.json().get('RetStatus', None)
                if status == 1: return 200, res
            return 400, res
        except Exception as e:
            log_error(e)
            return 400, str(e)

    def asend(self, _from, to, text, isflash=False):
        def send():
            try:
                requests.post(
                    url=self.send_endpoint,
                    data={
                        'username': self.username,
                        'password': self.password,
                        'from': _from,
                        'to': to,
                        'text': text,
                        'isflash': isflash
                    }
                )
            except Exception as e:
                log_error(e)
        Thread(target=send).start()

    def base_send(self, body_id, to, text):
        return requests.post(
            url=self.send_endpoint,
            data={
                'username': self.username,
                'password': self.password,
                'bodyId': body_id,
                'to': to,
                'text': text,
            }
        )


def send_welcome_sms(name, to, _from='50004001731036'):
    credents = settings.CREDENTIALS['melipayamak']
    sms = SMS(
        credents['username'],
        credents['password']
    )
    with open(settings.BASE_DIR / 'utils/sms_texts/welcome.txt', encoding='utf8') as f:
        text = f.read()
    return sms.send(_from, regulate_phone_number(to), text=text.format(name=name))
    # sms.base_send(104114, regulate_phone_number(to), text=text.format(name=name))
