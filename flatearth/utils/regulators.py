from .error_logger import log_error


def regulate_phone_number(phone_number):
    """
    0. 9999999999     : 10 digits
    1. 09999999999    : 11 digits
    2. 999999999999   : 12 digits
    3. +999999999999  : 12 digits + 1
    4. 00999999999999 : 14 digits
    """
    phone_number = str(phone_number)
    phone_number = phone_number.replace(' ', '')
    phone_number = phone_number.replace('-', '')

    if phone_number.startswith('+'):
        regulated_number = '00' + phone_number[1:]
        return regulated_number
    elif phone_number.startswith('00'):
        return phone_number

    if len(phone_number) == 10:
        regulated_number = '0098' + phone_number
    elif len(phone_number) == 11:
        regulated_number = '0098' + phone_number[1:]
    elif len(phone_number) == 12:
        regulated_number = '0098' + phone_number[2:]
    elif len(phone_number) == 13:
        regulated_number = '0098' + phone_number[3:]

    try:
        return regulated_number
    except Exception as e:
        log_error(e)
        raise Exception('invalid phone number')
